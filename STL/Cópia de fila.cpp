#include <iostream>
#include <cstdlib>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    int op;
    int num;
    list<int> l;
    list<int>::iterator aux; //interador- vari�vel que recebe o endere�o de um n�

    do
    {
        system("cls");
        
        cout<<"\n\nPratica - STL List";
        cout<<"\n\n\n 1- Recriar";
        cout<<"\n 2- Enqueue";
        cout<<"\n 3- Dequeue";
        cout<<"\n 4- Inicio";
        cout<<"\n\n 0- Sair";
        cout<<"\n\nInforme a opcao desejada: ";
        cin>>op;
        
        system("cls");
        switch(op)
        {
                case 1:                
                    //recriar a fila
                    l.clear();
                    cout<<"\n\nFila recriada com sucesso!!";
                break;
                case 2://incluir pelo final
                    cout<<"Informe o valor a ser inserido: ";                    
                    cin>>num;
                    l.push_back(num);
                    l.random_shuffle(num);                   
                    cout<<"Valor inserido com sucesso!!";                                                                                
                break;
                case 3://remover inicio
                    if (!l.empty()){
                       num = l.front(); //inicio da fila
                       l.pop_front();
                       cout<<"\n\nValor removido e: " <<num;
                    }
                    else {
                       cout<<"N�o � poss�vel remover - Fila vazia!!";                    
                    }
                    
                break;
                case 4: //Primeiro elemento
                    if (!l.empty()){
                       num = l.front(); //inicio da fila
                       cout<<"\n\nO primeiro elemento e: " <<num;
                    }
                    else {
                       cout<<"N�o � poss�vel consultar - Fila vazia!!";                    
                    }
                
                break;
                case 0: return 1;
                break;
                default: cout<<"Opcao incorreta"<<endl;
        }
        system("pause");
    }while(op!=0);
    
}
