#include <iostream>
#include <cstdlib>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    int op,valor;
    
    list<int> l;
    list<int>::iterator aux;

    do
    {
        system("cls");
        
        cout<<"\n\nPratica - STL List";
        cout<<"\n\n\n 1- Reriar";
        cout<<"\n 2- Incluir no inicio";
        cout<<"\n 3- Incluir no final";
        cout<<"\n 4- Remover pelo inicio";
        cout<<"\n 5- Remover pelo final";
        cout<<"\n 6- Consultar";
        cout<<"\n 7- Numero de elementos";
        cout<<"\n 8- Numero maximo de elementos";
        cout<<"\n 9- Primeiro elemento";
        cout<<"\n10- Ultimo elemento";
        cout<<"\n11- Remover elemento especifico";
        cout<<"\n12- Remover elementos repetidos";
        cout<<"\n13- Inverte a ordem dos elementos";
        cout<<"\n14- Ordena os elementos";
        cout<<"\n\n 0- Sair";
        cout<<"\n\nInforme a opcao desejada: ";
        cin>>op;
        
        system("cls");
        switch(op)
        {
                case 1: l.clear();
                        cout<<endl<<"Lista reinicializada";
                        system("pause");
                break;
                case 2: cout<<endl<<"Numero para inserir: ";
                        cin>>valor;
                        l.push_front(valor);
                        cout<<endl<<"Valor inserido com sucesso";
                        //system("pause");
                break;
                case 3: cout<<endl<<"Numero para inserir: ";
                        cin>>valor;
                        l.push_back(valor);
                        cout<<endl<<"Valor inserido com sucesso";
                        //system("pause");
                break;
                case 4: if(!l.empty()) {
                            cout<<endl<<"Valor removido: "<<l.front();
                            l.pop_front();
                        }    
                        else
                            cout<<endl<<"Lista vazia";
                        system("pause");
                break;
                case 5: if(!l.empty()) {
                            cout<<endl<<"Valor removido: "<<l.front();
                            l.pop_back();
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 6: if(!l.empty()) {
                            cout<<endl<<"Elementos da lista"<<endl;
                            for(aux = l.begin();aux != l.end(); aux++)
                                cout<<endl<<*aux;
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 7: cout<<endl<<"Numero de elementos: "<<l.size()<<endl;
                        system("pause");
                break;
                case 8: cout<<endl<<"Numero maximo de elementos: "<<l.max_size()<<endl;
                        system("pause");
                break;
                case 9: if(!l.empty()) {
                            cout<<endl<<"Primeiro elemento: "<<l.front();
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 10: if(!l.empty()) {
                            cout<<endl<<"Ultimo elemento: "<<l.back()<<endl;
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 11: if(!l.empty()) {
                            cout<<endl<<"Numero para remover: ";
                            cin>>valor;
                            aux = find(l.begin(),l.end(),valor);   //l.end = NULL
                            if(aux != l.end()) {
                                l.remove(valor);
                                cout<<endl<<"Elemento removido com sucesso"<<endl;
                            }
                            else
                                cout<<endl<<"Elemento nao encontrado"<<endl;
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 12: if(!l.empty()) {
                             l.unique();
                             cout<<endl<<"Elementos repetidos removidos "<<endl;
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                break;
                case 13: if(!l.empty()) {
                             l.reverse();
                             cout<<endl<<"Elementos invertidos com sucesso: "<<endl;
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                break;
                case 14: if(!l.empty()) {
                            l.sort();
                            cout<<endl<<"Lista ordenada com sucesso: "<<endl;
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 0: return 1;
                break;
                default: cout<<"Opcao incorreta"<<endl;
                         system("pause");
        }
    }while(op!=0);
    
}
