#include <iostream>
#include <cstdlib>
#include <list>
#include <algorithm>

using namespace std;

class no
{
    private:
        int cod;
        string nome;
        int idade;

    public:
        no();
        no(int cod, string nome, int idade);
        ~no();
        int getcod();
        void setcod(int cod);
        string getnome();
        void setnome(string nome);
        int getid();
        void setid(int id);
        void lista();
};

no::~no()
{
    cout<<endl<<"Removido: ";
    lista();
} 

no::no()
{

} 

no::no(int cod, string nome, int idade)
{
    this->cod = cod;
    this->nome = nome;
    this->idade = idade;
}

int no::getcod()
{
    return cod;
}

void no::setcod(int cod)
{
    this->cod = cod;
}


string no::getnome()
{
    return nome;
}

void no::setnome(string nome)
{
    this->nome = nome;
}

int no::getid()
{
    return idade;
}

void no::setid(int id)
{
    this->idade = id;
}


void no::lista()
{
    cout<<endl<<"Codigo: "<<cod;
    cout<<endl<<"Nome: "<<nome;
    cout<<endl<<"Idade: "<<idade<<endl;
}


int main()
{
    int op,cod,id;
    string nome;
    no pessoa;
    no cad;
    
    list<no> l;
    list<no>::iterator aux,aux2;

    do
    {
        system("cls");
        
        cout<<"\n\nPratica - STL List (Resolucao de Problemas)";
        cout<<"\n\n\n 1- Reriar";
        cout<<"\n 2- Incluir no inicio";
        cout<<"\n 3- Incluir no final";
        cout<<"\n 4- Remover pelo inicio";
        cout<<"\n 5- Remover pelo final";
        cout<<"\n 6- Consultar";
        cout<<"\n 7- Numero de elementos";
        cout<<"\n 8- Numero maximo de elementos";
        cout<<"\n 9- Primeiro elemento";
        cout<<"\n10- Ultimo elemento";
        cout<<"\n11- Remover elemento especifico";
        cout<<"\n12- Remover elementos repetidos";
        cout<<"\n13- Inverte a ordem dos elementos";
        cout<<"\n14- Ordena os elementos";
        cout<<"\n15- Consultar especifico";
        cout<<"\n16- Trocar a posicao de dois nos";
        cout<<"\n\n 0- Sair";
        cout<<"\n\nInforme a opcao desejada: ";
        cin>>op;
        
        system("cls");
        switch(op)
        {
                case 1: l.clear();
                        cout<<endl<<"Lista reinicializada";
                        system("pause");
                break;
                case 2: cout<<endl<<"Codigo: ";
                        cin>>cod;
                        cout<<endl<<"Nome: ";
                        cin>>nome;
                        cout<<endl<<"Idade: ";
                        cin>>id;
                        
                        pessoa.setcod(cod);
                        pessoa.setnome(nome);
                        pessoa.setid(id);
                        
                        l.push_front(pessoa);
                        
                        cout<<endl<<"Valor inserido com sucesso";
                        //system("pause");
                break;
                case 3: cout<<endl<<"Codigo: ";
                        cin>>cod;
                        cout<<endl<<"Nome: ";
                        cin>>nome;
                        cout<<endl<<"Idade: ";
                        cin>>id;
                        
                        pessoa.setcod(cod);
                        pessoa.setnome(nome);
                        pessoa.setid(id);
                        
                        l.push_back(pessoa);
                        
                        cout<<endl<<"Valor inserido com sucesso";
                        //system("pause");
                break;
                case 4: if(!l.empty()) {
                            pessoa = l.front();
                            cout<<endl<<"Elemento removido"<<endl;
                            pessoa.lista();
                            l.pop_front();
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 5: if(!l.empty()) {
                            pessoa = l.back();
                            cout<<endl<<"Elemento removido"<<endl;
                            pessoa.lista();
                            l.pop_back();
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 6: if(!l.empty()) {
                            cout<<endl<<"Elementos da lista"<<endl;
                            for(aux = l.begin();aux != l.end(); aux++)
                                aux->lista();
                                cout<<endl;
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 7: cout<<endl<<"Numero de elementos: "<<l.size()<<endl;
                        system("pause");
                break;
                case 8: cout<<endl<<"Numero maximo de elementos: "<<l.max_size()<<endl;
                        system("pause");
                break;
                case 9: if(!l.empty()) {
                            pessoa = l.front();
                            cout<<endl<<"Primeiro elemento: "<<endl;
                            pessoa.lista();
                        }    
                        else
                            cout<<endl<<"Lista vazia"<<endl;
                        system("pause");
                break;
                case 10: if(!l.empty()) {
                             pessoa = l.back();
                             cout<<endl<<"Ultimo elemento: "<<endl;
                             pessoa.lista();
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                break;
                case 11: if(!l.empty()) {
                             cout<<endl<<"Codigo: ";
                             cin>>cod;
                             aux = l.begin();
                             while((aux!=l.end()) && (cod!=aux->getcod()))
                                 aux++;
                             if (aux!=l.end())
                                 l.erase(aux);
                             else
                                 cout<<endl<<"Elemento nao encontrado";
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                break;
                case 12: cout<<"Nao funciona neste container";
                         system("pause");
                break;
                case 13: if(!l.empty()) {
                             l.reverse();
                             cout<<endl<<"Elementos invertidos com sucesso: "<<endl;
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                break;
                case 14: cout<<"Nao funciona neste container";
                         system("pause");
                break;
                case 15: if(!l.empty()) {
                             cout<<endl<<"Codigo: ";
                             cin>>cod;
                             aux = l.begin();
                             while((aux!=l.end()) && (cod!=aux->getcod()))
                                 aux++;
                             if (aux!=l.end())
                                 aux->lista();
                             else
                                 cout<<endl<<"Elemento nao encontrado";
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                    
                    
                         
                break;
                case 16: if(!l.empty()) {
                             cout<<endl<<"Codigo do primeiro no: ";
                             cin>>cod;
                             aux = l.begin();
                             while((aux!=l.end()) && (cod!=aux->getcod()))
                                 aux++;
                             if (aux!=l.end()) {
                                 cout<<endl<<"Codigo do segundo no: ";
                                     cin>>cod;
                                 aux2 = l.begin();
                                 while((aux2!=l.end()) && (cod2!=aux2->getcod()))
                                     aux2++;
                                 if (aux2!=l.end())
                                     cad = *aux;
                                     *aux = *aux2;
                                     *aux2 = cad;
                             }    
                             else
                                 cout<<endl<<"Elemento nao encontrado";
                         }    
                         else
                             cout<<endl<<"Lista vazia"<<endl;
                         system("pause");
                
                break;
                case 0: return 1;
                break;
                default: cout<<"Opcao incorreta"<<endl;
                         system("pause");
        }
    }while(op!=0);
    
}
