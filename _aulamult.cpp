#include <iostream>
#include <cstdlib>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    int op, i;
    int valor, sub;
    
    list<list<int> > ml;
    list<int> l;
    list<list<int> >::iterator auxml;
    list<int>::iterator auxl;
       
    do
    {
        system("cls");
        
        cout<<"\n\nPratica - STL List";
        cout<<"\n\n\n 1- Reriar (5 multilistas)";
        cout<<"\n 2- Incluir no inicio (informar em qual sublista)";
        cout<<"\n 3- Incluir no final (informar em qual sublista)";
        cout<<"\n 4- Remover pelo inicio (informar em qual sublista)" ;
        cout<<"\n 5- Remover pelo final (informar em qual sublista)";
        cout<<"\n 6- Remover específico (elemento)";
        cout<<"\n 7- Remover sublista";
        cout<<"\n 8- Remover multilista";
        cout<<"\n 9- Consultar mulilista";
        cout<<"\n 10- Consultar primeiro da mulilista";
        cout<<"\n 11- Consultar ultimo da mulilista";
        cout<<"\n 12- Consultar primeiro da sublista (informar qual sublista)";
        cout<<"\n 13- Consultar ultimo da sublista (informar qual sublista)";
        cout<<"\n 14- Numero de elementos da multilista";
        cout<<"\n 15- Numero de elementos de cada sublista";
        cout<<"\n 16- Inverter ordem da multilista";
        cout<<"\n 17- Inverter ordem da sublista (informar qual)";
        cout<<"\n 18- Trocar dois elementos m e n de posicao na multilista";
        cout<<"\n 19- Localizar um elemento na multilista";
        cout<<"\n 20- Remover elementos repetidos (de cada sublista)";
        cout<<"\n 21- Ordenar elementos (de cada sublista)";
        cout<<"\n 22- Alterar um elementos";
        cout<<"\n\n 0- Sair";
        cout<<"\n\nInforme a opcao desejada: ";
        cin>>op;
        
        system("cls");
        switch(op)
        {
            case 1: ml.clear();
                    for (i=1;i<6;i++)
                    {
                        ml.push_back(l);
                    }
                    cout<<endl<<"Multilista recriada com sucesso";        
                    system("pause");
            break;
            
            case 2: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub <= ml.size())
                    {
                        cout<<endl<<"Informe o valor: ";
                        cin>>valor;
                        auxml = ml.begin();
                        for (i=1;i<sub;i++)
                        {
                            auxml++;
                        }
                    
                        auxml->push_front(valor);
                        cout<<endl<<"Valor inserido com sucesso";        
                    }    
                    else
                        cout<<endl<<"Sublista incorreta";
            break;
            
            case 3: cout<<endl<<"Informe a sublista a ser utilizada: ";
                    cin>>sub;
                    if (sub <= ml.size())
                    {
                        cout<<endl<<"Informe o valor: ";
                        cin>>valor;
                        auxml = ml.begin();
                        for (i=1;i<sub;i++)
                        {
                            auxml++;
                        }
                    
                        auxml->push_back(valor);
                        cout<<endl<<"Valor inserido com sucesso";        
                    }    
                    else
                        cout<<endl<<"Sublista incorreta";
            break;
            
            case 4: if (!ml.empty())
                    {
                        cout<<endl<<"Informe a sublista a ser utilizada: ";
                        cin>>sub;
                    
                        if (sub < ml.size()+1)
                        {
                            auxml = ml.begin();
                            for (i=1;i<sub;i++)
                                auxml++;
                            if (!auxml->empty()) {
                                cout<<endl<<"Valor removido: "<<auxml->front();        
                                auxml->pop_front();
                                cout<<endl<<"Valor removido com sucesso";   
                            }    
                            else
                                cout<<endl<<"Sublista vazia";         
                        }    
                    }    
                    else
                        cout<<endl<<"Sublista incorreta";
                    system("pause");
            break;
                
            case 5: if (!ml.empty())
                    {
                        cout<<endl<<"Informe a sublista a ser utilizada: ";
                        cin>>sub;
                    
                        if (sub < ml.size()+1)
                        {
                            auxml = ml.begin();
                            for (i=1;i<sub;i++)
                                auxml++;
                            if (!auxml->empty()) {
                                cout<<endl<<"Valor removido: "<<auxml->back();        
                                auxml->pop_back();
                                cout<<endl<<"Valor removido com sucesso";   
                            }    
                            else
                                cout<<endl<<"Sublista vazia";    
                        }    
                    }    
                    else
                        cout<<endl<<"Sublista incorreta";
                    system("pause");
            break;    
            
            case 6: if (!ml.empty())
                    {
                        cout<<endl<<"Informe o elemento a ser removido: ";
                        cin>>valor;
                        
                        auxml = ml.begin();
                        while(auxml != ml.end())             
                        {
                            auxl = find(auxml->begin(),auxml->end(),valor);
                            if (auxl != auxml->end())
                                break;
                            auxml++;
                        }    
                        if (auxml != ml.end())
                            auxml->erase(auxl);
                        else
                            cout<<endl<<"Elemento nao encontrado";
                    }    
                    else
                        cout<<endl<<"Sublista incorreta";
                    system("pause");
            break;
                
            case 9: cout<<endl<<"Multilista"<<endl<<endl;
                    for(auxml=ml.begin();auxml!=ml.end();auxml++) 
                    {
                        if(!auxml->empty())
                        {
                            for(auxl=auxml->begin();auxl!=auxml->end();auxl++)    
                                cout<<*auxl<<"     "; 
                            cout<<endl;   
                        }    
                        else
                            cout<<endl<<"Sublista vazia"<<endl;
                    }    
                    system("pause");
            break;
            
            case 12: if (!ml.empty())
                         cout<<endl<<"Informe a sublista: ";
                         cin>>sub;
                         if (sub < ml.size()+1)
                         {
                             auxml = ml.begin();
                             for (i=1;i<sub;i++)
                                 auxml++;
                             if (!auxml->empty())
                                 cout<<endl<<"Primeiro elemento da sublista "<<i<<": "<<auxml->front();
                             else
                                 cout<<endl<<"Sublista vazia";
                         }    
                         else
                             cout<<endl<<"Sublista inexistente";
                     system("pause");
            break;
            
            case 13: if (!ml.empty())
                         cout<<endl<<"Informe a sublista: ";
                         cin>>sub;
                         if (sub < ml.size()+1)
                         {
                             auxml = ml.begin();
                             for (i=1;i<sub;i++)
                                 auxml++;
                             if (!auxml->empty())
                                 cout<<endl<<"Ultimo elemento da sublista "<<i<<" : "<<auxml->back();
                             else
                                 cout<<endl<<"Sublista vazia";
                         }    
                         else
                             cout<<endl<<"Sublista inexistente";
                     system("pause");
            break;
            
            case 14: if (!ml.empty())
                         cout<<endl<<"Numero de elementos da multilista: "<<ml.size();
                     else
                         cout<<endl<<"Multilista vazia";
                     system("pause");
            break;
            
            case 15: if (!ml.empty()) 
                     {
                         cout<<endl<<"Numero de elementos das sublistas";
                         auxml = ml.begin();
                         for (i=1;i<6;i++)
                         {
                             cout<<endl<<"Sublista "<<i<<": "<<auxml->size();
                             auxml++;
                         }       
                     }    
                     else
                         cout<<endl<<"Multilista vazia";
                     system("pause");
            break;
            
            case 22: if (!ml.empty())
                     {
                         cout<<endl<<"Informe o elemento a ser alterado: ";
                         cin>>valor;
                         
                         auxml = ml.begin();
                         while(auxml != ml.end())             
                         {
                             auxl = find(auxml->begin(),auxml->end(),valor);
                             if (auxl != auxml->end())
                                 break;
                             auxml++;
                         }    
                         if (auxml != ml.end())
                         {
                             cout<<endl<<"Informe o novo valor: ";
                             cin>>valor;
                             *auxl = valor;
                         }    
                         else
                             cout<<endl<<"Elemento nao encontrado";
                     }    
                     else
                         cout<<endl<<"Sublista incorreta";
                     system("pause");
            break;
            
            case 0: return 1;
            break;
            default: cout<<"Opcao incorreta"<<endl;
                     system("pause");
        }
    }while(op!=0);
    
}
